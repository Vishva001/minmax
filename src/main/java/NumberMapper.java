import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Random;

public class NumberMapper extends Mapper<Object, Text, Text, LongWritable> {

    public void map(Object InputKey, Text InputValue, Context context) throws IOException, InterruptedException {
        System.out.println(InputKey+ ":" + InputValue);
        Text outKey = new Text();
        outKey.set("NumJob" + new Random().nextInt(3));
        //outKey.set("NUMS");
        LongWritable ll = new LongWritable();
        ll.set(Long.valueOf(InputValue.toString()));
        context.write(outKey,ll);
    }
}
