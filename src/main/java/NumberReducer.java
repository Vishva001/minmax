import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class NumberReducer extends Reducer<Text, LongWritable, Text, LongWritable> {

    public enum minMaxHoldingReducerCounters {
        MIN,
        MAX,
        MIN_DEFAULT
    }

    public void reduce(Text inputKey, Iterable<LongWritable> inputValues, Context context) throws IOException, InterruptedException {

        if(context.getCounter(minMaxHoldingReducerCounters.MIN_DEFAULT).getValue() == 0)
        {
            context.getCounter(minMaxHoldingReducerCounters.MAX).setValue(Integer.MIN_VALUE);
            context.getCounter(minMaxHoldingReducerCounters.MIN).setValue(Integer.MAX_VALUE);
            context.getCounter(minMaxHoldingReducerCounters.MIN_DEFAULT).setValue(-1);
        }

        for(LongWritable val : inputValues)
        {
            if(val.get() < context.getCounter(minMaxHoldingReducerCounters.MIN).getValue())
            {
                context.getCounter(minMaxHoldingReducerCounters.MIN).setValue(val.get());
            }
            if(val.get() > context.getCounter(minMaxHoldingReducerCounters.MAX).getValue())
            {
                context.getCounter(minMaxHoldingReducerCounters.MAX).setValue(val.get());
            }
        }

        writeOutput("MIN", context.getCounter(minMaxHoldingReducerCounters.MIN).getValue(), context);
        writeOutput("MAX", context.getCounter(minMaxHoldingReducerCounters.MAX).getValue(), context);
    }

    private void writeOutput(String key, Long value, Context c) throws IOException, InterruptedException {
        Text writeKey = new Text();
        writeKey.set(key);

        LongWritable writeValue = new LongWritable();
        writeValue.set(value);

        c.write(writeKey, writeValue);
    }
}
