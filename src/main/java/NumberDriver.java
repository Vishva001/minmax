import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

public class NumberDriver extends Configured implements Tool {

    @Override
    public int run(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = setup(args);
        return (job.waitForCompletion(true) ? 0: 1);
    }

    public Job setup(String[] args) throws IOException {
        Configuration conf = new Configuration();
        Job job = getJobObject(conf);
        System.out.println("outputpath : " + args[1]);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        return job;
    }

    public Job getJobObject(Configuration conf) throws IOException {
        Job job = Job.getInstance(conf, "NumberDriver");
        job.setJarByClass(NumberDriver.class);
        job.setMapperClass(NumberMapper.class);
        job.setReducerClass(NumberReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        return job;

    }

    public static void main(String[] args) throws Exception{
        ToolRunner.run(new NumberDriver(), args);
    }

}
